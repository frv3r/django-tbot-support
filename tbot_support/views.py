import traceback

import ujson
from aiogram.types import Update
from django.http import HttpResponse, JsonResponse
from loguru import logger

from .bot import dp
from .manager import support_manager as sm


async def handler(request):
    if not sm.is_connected():
        await sm.init()

    d = {}
    async for i in sm.iter_dialogs():
        if i.name.startswith('support_chat'):
            d[i.id] = i.name
    return JsonResponse(d)


async def webhook(request):
    if request.META['CONTENT_TYPE'] == 'application/json':
        json_data = request.body.decode('utf-8')
        update = Update.to_object(ujson.loads(json_data))
        try:
            await dp.process_update(update)
        except Exception as e:
            logger.error(traceback.format_exc())

        return HttpResponse(status=200)
    else:
        return HttpResponse(status=402)


# gunicorn django_tbot_support.asgi -k uvicorn.workers.UvicornWorker
