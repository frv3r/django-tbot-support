from aiogram import types
from aiogram.dispatcher import FSMContext
from loguru import logger
from telethon import TelegramClient
from telethon.errors import (FloodWaitError, PhoneCodeExpiredError,
                             PhoneCodeInvalidError, SessionPasswordNeededError)
from telethon.sessions import MemorySession, StringSession
from telethon.utils import get_display_name

from tbot_support.manager import support_manager
from tbot_support.models import Operator, Ticket, TicketMessage, TelegramAPI

from .bot import bot, dp

clients = {}


def numeric_keyboard():
    markup = {
        'inline_keyboard': [
            [{'text': '1', 'callback_data': 'num_1'}, {'text': '2', 'callback_data': 'num_2'}, {'text': '3', 'callback_data': 'num_3'}],
            [{'text': '4', 'callback_data': 'num_4'}, {'text': '5', 'callback_data': 'num_5'}, {'text': '6', 'callback_data': 'num_6'}],
            [{'text': '7', 'callback_data': 'num_7'}, {'text': '8', 'callback_data': 'num_8'}, {'text': '9', 'callback_data': 'num_9'}],
            [{'text': '<', 'callback_data': 'num_-'}, {'text': '0', 'callback_data': 'num_0'}, {'text': ' ', 'callback_data': 'frv3r'}],
        ]
    }
    markup = types.InlineKeyboardMarkup.to_object(markup)
    return markup


async def get_or_create_client(user_id: int) -> TelegramClient | None:
    """
        Функция создающая экземпляр клиента и помещающая его в память.
        Если в памяти уже есть привязанный к юзеру клиент, возвращает его.
    """
    # проверяем наличие открытого клиента в памяти
    if exists_client := clients.get(user_id):
        return exists_client

    tg_settings = TelegramAPI.objects.first()
    if tg_settings:
        client = TelegramClient(MemorySession(), tg_settings.api_id, tg_settings.api_hash)
        clients[user_id] = client
        await client.connect()
        return client
    else:
        return


@dp.message_handler(commands=['start'])
async def start_handler(message, state: FSMContext):
    user_id = message.from_user.id
    await state.set_data({})

    try:
        Operator.objects.get(user_id=user_id)
    except Operator.DoesNotExist:
        await bot.send_message(
            user_id, 'Щоб пройти авторизацію, будь ласка, надішліть ваш номер',
            reply_markup=types.ReplyKeyboardMarkup(resize_keyboard=True).add(
                types.KeyboardButton('Поділітися контактом', request_contact=True))
        )


@dp.message_handler(content_types=['contact'])
async def contact_handler(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    if message.contact.user_id == user_id:
        try:
            operator = Operator.objects.get(phone=message.contact.phone_number.lstrip('+'))
        except Operator.DoesNotExist:
            await bot.send_message(user_id, 'Ваш акаунт не знайдено в базі даних')
        else:
            if operator.is_registered:
                return await bot.send_message(user_id, 'ok')

            client = await get_or_create_client(message.from_user.id)
            if not client:
                return await bot.send_message(user_id, 'TelegramAPI не налаштовано!')

            try:
                req = await client.send_code_request(message.contact.phone_number)
            except FloodWaitError:
                return await bot.send_message(user_id, 'Кількість спроб зареєструватися вичерпано (флудвейт)')

            await state.update_data({
                'PHONE': message.contact.phone_number,
                'PHONE_HASH': req.phone_code_hash
            })

            await bot.send_message(
                user_id, 'Введіть код', reply_markup=numeric_keyboard()
            )

            await state.set_state('code')
            await state.update_data({'model': operator, 'code': ''})
    else:
        await bot.send_message(user_id, 'Ви надіслали не свій контакт. Взагалі не смішно.')


@dp.callback_query_handler(state='code')
async def num_code_handler(call: types.CallbackQuery, state: FSMContext):
    _, num = call.data.split('_')
    entered = (await state.get_data())['code']
    if num == '-':
        entered = entered[:-1]
    else:
        entered += num

    await bot.edit_message_text('Введіть код\n\n'
                                f'<b>Ви ввели:</b> <code>{entered}</code>',
                                chat_id=call.from_user.id,
                                message_id=call.message.message_id,
                                parse_mode='html',
                                reply_markup=numeric_keyboard())

    await state.update_data({'code': entered})
    if len(entered) == 5:
        await state.set_state(None)
        await proceed_code(call.from_user.id, state, entered)


@dp.message_handler(state='code')
async def use_keyboard_message(message: types.Message):
    await bot.send_message(message.from_user.id, 'Використовуйте клавіатуру')


@dp.message_handler(state='password')
async def password_handler(message: types.Message, state: FSMContext):
    code = (await state.get_data())['code']
    await proceed_code(message.from_user.id, state, code, message.text)


async def proceed_code(user_id, state, code, password=None):
    state_data = await state.get_data()
    logger.debug(state_data)

    operator = state_data['model']
    phone = state_data['PHONE']
    phone_code_hash = state_data['PHONE_HASH']

    client = await get_or_create_client(user_id)
    if not client:
        return await bot.send_message(user_id, 'Напишіть /start і спробуйте ще раз')

    try:
        if password:
            await client.sign_in(password=password)
        else:
            await client.sign_in(phone, code, phone_code_hash=phone_code_hash)
    except PhoneCodeInvalidError:
        return await bot.send_message(user_id, 'Ви ввели невірний код. Спробуйте ще раз')
    except PhoneCodeExpiredError:
        return await bot.send_message(user_id, 'Код не дійсний. Напишіть /start і спробуйте ще раз')
    except SessionPasswordNeededError:
        await state.set_state('password')
        return await bot.send_message(user_id, 'Введіть пароль')

    support_manager.operator_clients.update({
        user_id: client
    })
    string_session = StringSession.save(client.session)

    operator.user_id = user_id
    operator.string_session = string_session
    operator.is_registered = True
    operator.name = get_display_name(await client.get_me())
    operator.save()

    if operator.role == 'core':
        text = 'Ви зареєстрували цей акаунт як бота що буде створювати чати для опрацювання тікетів'
    else:
        text = 'Ви успішно зареєструвалися як оператор підтримки, очікуйте на надходження тікетів'
    await bot.send_message(user_id, text)
    await state.set_data({})


@dp.callback_query_handler(lambda call: call.data.startswith('ticket'))
async def in_work_handler(call: types.CallbackQuery):
    logger.debug(call)
    _, ticket_id = call.data.split('_')
    ticket = Ticket.objects.get(id=ticket_id)
    if ticket.operator:
        return await bot.edit_message_text(
            'Цей тікет вже на опрацюванні іншим оператором!',
            call.from_user.id, call.message.message_id
        )

    try:
        operator = Operator.objects.get(user_id=call.from_user.id)
    except Operator.DoesNotExist:
        return await bot.send_message(call.from_user.id, 'Ви не зареєстровані як оператор')

    await support_manager.assign_operator(ticket, operator)
    sent_messages = TicketMessage.objects.filter(ticket=ticket).exclude(operator_id=call.from_user.id)
    await bot.edit_message_text(
        call.message.text + '\n\n' +
        '<b>Ви прийняли цей тікет на опрацювання</b>',
        call.from_user.id, call.message.message_id,
        parse_mode='html'
    )
    for sent_message in sent_messages:
        try:
            await bot.edit_message_reply_markup(
                sent_message.ticket.operator.user_id, sent_message.message_id
            )
        except Exception as e:
            logger.debug(e)
    sent_messages.delete()


@support_manager.new_ticket_handler
async def new_ticket_handler(ticket):
    operators = Operator.objects.filter(is_registered=True, is_active=True)
    markup = types.InlineKeyboardMarkup().add(
        types.InlineKeyboardButton(
            'В роботу', callback_data=f'ticket_{ticket.id}'
        )
    )
    ticket_messages = []
    for op in operators:
        sent_message = await bot.send_message(
            op.user_id,
            f'Ticket: {ticket.id}\n'
            f'Message: {ticket.message_text}\n'
            f'Client: {ticket.user_id}',
            reply_markup=markup
        )
        ticket_messages.append(TicketMessage(
            ticket=ticket,
            operator=op,
            message_id=sent_message.message_id,
        ))
    TicketMessage.objects.bulk_create(ticket_messages)
