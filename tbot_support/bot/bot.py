import asyncio

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from loguru import logger

from tbot_support.models import SupportBot

try:
    b = SupportBot.objects.first()
except Exception as e:
    logger.debug(e)
    token = '332211:11122'
    webhook = None
else:
    if b:
        token = b.token
        webhook = b.webhook
    else:
        token = '332211:11122'
        webhook = None

loop = asyncio.get_event_loop()
bot = Bot(token, loop=loop, validate_token=False)
dp = Dispatcher(bot, loop=loop, storage=MemoryStorage())


async def on_startup():
    logger.debug('settig hook')
    await bot.set_webhook(webhook)

loop.create_task(on_startup())

logger.debug('support bot inited')

