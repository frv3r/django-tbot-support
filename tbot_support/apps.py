from django.apps import AppConfig


class TbotSupportConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tbot_support'
    verbose_name = 'Техпідтримка'
