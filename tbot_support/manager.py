import asyncio
import time

from asgiref.sync import async_to_sync
from loguru import logger
from telethon import TelegramClient, events
from telethon.sessions import StringSession
from telethon.tl import functions
from telethon.tl.functions.contacts import ImportContactsRequest
from telethon.tl.functions.messages import (AddChatUserRequest,
                                            GetDialogFiltersRequest,
                                            UpdateDialogFilterRequest)
from telethon.tl.types import (Chat, DialogFilter, InputPeerChat,
                               InputPhoneContact)

from tbot_support.models import Operator, TelegramAPI, Ticket, UserMessage


class SupportManager(TelegramClient):
    def __init__(self, **kwargs):
        self.answer_handlers = []
        self.new_ticket_handlers = []
        self.operator_clients = {}

        tg_settings = TelegramAPI.objects.first()
        if tg_settings:
            self.API_ID = tg_settings.api_id
            self.API_HASH = tg_settings.api_hash
        else:
            self.API_ID = 420
            self.API_HASH = 'frv3r'

        try:
            core = Operator.objects.get(role='core')
        except Operator.DoesNotExist:
            logger.warning('CORE OPERATOR IS NOT SET')
            return
        else:
            super().__init__(
                StringSession(core.string_session), self.API_ID, self.API_HASH,
                **kwargs
            )

        for operator in Operator.objects.all().exclude(role='core'):
            self.operator_clients[operator.user_id] = TelegramClient(
                StringSession(operator.string_session), self.API_ID, self.API_HASH
            )

    def check_connection(coro):
        async def wrapper(self, *args, **kwargs):
            if not self.is_connected():
                await self.init()
            return await coro(self, *args, **kwargs)
        return wrapper

    async def init(self):
        logger.debug('Connecting to telegram...')
        await self.connect()

        self.add_event_handler(self.command_handler, events.NewMessage)
        self.add_event_handler(self.reply_handler, events.NewMessage)
        self.add_event_handler(self.invite_handler, events.ChatAction)

    def new_ticket_handler(self, func):
        self.new_ticket_handlers.append(func)
        return func

    def answer_handler(self, func):
        self.answer_handlers.append(func)
        return func

    def question_handler(self, func):
        @async_to_sync
        async def wrapper(message):
            user_id = message.from_user.id
            try:
                ticket = Ticket.objects.get(user_id=user_id, is_ended=False, support_chat_id__isnull=False)
            except Ticket.DoesNotExist:
                is_new_ticket = True

                ticket = Ticket.objects.create(user_id=user_id, message_text=message.text)
                UserMessage.objects.create(ticket=ticket, user_message_id=message.message_id)
                for new_ticket_handler in self.new_ticket_handlers:
                    await new_ticket_handler(ticket)
            else:
                is_new_ticket = False

                if message.reply_to_message:
                    chat_message = UserMessage.objects.get(
                        ticket=ticket,
                        user_message_id=message.reply_to_message.message_id
                    )
                    reply_to_message_id = chat_message.support_message_id
                else:
                    reply_to_message_id = None
                # todo: media
                sent_message = await self.send_message(
                    ticket.support_chat_id, message.text, reply_to=reply_to_message_id
                )
                UserMessage.objects.create(
                    ticket=ticket, support_message_id=sent_message.id,
                    user_message_id=message.message_id
                )
            finally:
                func(message, is_new_ticket)

        return wrapper

    async def invite_handler(self, event: events.chataction.ChatAction.Event):
        if not event.user_added:
            return

        chat = await event.get_chat()
        added_by = event.added_by
        user = event.users[0]
        operator = Operator.objects.filter(user_id=user.id).first()
        if operator:
            ticket = Ticket.objects.get(support_chat_id=-abs(chat.id))
            await self.assign_operator(ticket, operator)
            await self.send_message(chat.id, f'Оператор тикета теперь {user.first_name}')
        else:
            await self.kick_participant(chat, user)
            await self.send_message(chat.id,
                                    f'Осторожнее, {added_by.first_name}, нельзя добавлять в такие чаты левых персонажей!')

    async def command_handler(self, event):
        message = event.message
        if not message.text.startswith('.'):
            return

        chat = await event.get_chat()
        ticket = Ticket.objects.filter(support_chat_id=-abs(chat.id)).last()
        client_user = await self.get_entity(ticket.user_id)

        match message.text:
            case '.status' | '.s':
                text = f'Користувач: {client_user.first_name} ' \
                       f'{"@" + client_user.username if client_user.username else ""}\n' \
                       f'Оператор: {ticket.operator_id}\n\n' \
                       f'Питання користувача: <code>{ticket.message_text}</code>\n' \
                       f'Статус: {"Тікет закритий" if ticket.is_ended else "Тікет відкритий"}'
                return await event.respond(text, parse_mode='html')
            case '.dump' | '.d':
                return await event.respond((await message.get_reply_message()).stringify())
            case '.end' | '.e':
                if ticket.is_ended:
                    return await event.respond('Тікет вже завершено')

                ticket.is_ended = True
                ticket.save()
                UserMessage.objects.filter(ticket=ticket).delete()

                async for operator_user in self.iter_participants(chat):
                    if operator_user.is_self:
                        continue

                    operator_client = self.operator_clients[operator_user.id]
                    await self._append_or_create_folder(
                        client=operator_client,
                        folder_name='Closed',
                        chat=chat
                    )
                    await self._pop_or_delete_folder(
                        client=operator_client,
                        folder_name='Opened',
                        chat=chat
                    )

                await self.send_message(chat, 'Разговор завершен')
            case '.history' | '.h':
                messages = await self.get_dialog_story(ticket)
                text = ''
                self_id = (await self.get_me()).id
                for m in messages:
                    logger.debug(m.from_id)
                    text += f'{"<b>CLIENT:</b>" if m.from_id.user_id == self_id else "<b>OP:</b>"} {m.text}\n'
                await self.send_message(chat, text, parse_mode='html')

    async def reply_handler(self, event):
        message = event.message
        ticket = Ticket.objects.filter(support_chat_id=-abs(event.chat_id)).last()

        if message.is_reply:
            if ticket.is_ended:
                return await event.respond('Цей тікет закритий')

            reply_message_id = message.reply_to.reply_to_msg_id
            try:
                umessage = UserMessage.objects.get(
                    ticket=ticket,
                    support_message_id=reply_message_id
                )
            except UserMessage.DoesNotExist:
                return await event.respond('Відповідь не надіслана')

            for func in self.answer_handlers:
                sent_message_id = func(umessage.ticket.user_id, umessage.user_message_id, message.text)
                UserMessage.objects.create(
                    ticket=ticket,
                    support_message_id=message.id,
                    user_message_id=sent_message_id
                )

    @check_connection
    async def assign_operator(self, ticket: Ticket, operator: Operator):
        if ticket.operator_id == operator.id:
            return False

        try:
            operator_user = await self.get_entity(operator.user_id)
        except ValueError:
            await self.get_dialogs()
            contact = InputPhoneContact(0, operator.phone, 'Operator', str(operator.id))
            await self(ImportContactsRequest([contact]))
            operator_user = await self.get_entity(operator.user_id)

        chat, is_new_chat = await self._get_or_create_chat(
            operator_user,
            ticket.user_id
        )
        logger.debug(chat)

        if is_new_chat:
            await self.send_message(chat, 'Был принят тикет')
            # fixme:
            await self._append_or_create_folder(self.operator_clients[operator.user_id], 'Opened', chat)
        else:
            if operator_user not in await self.get_participants(chat):
                await self(AddChatUserRequest(
                    chat.input_entity.chat_id, operator_user.id, fwd_limit=100
                ))
            await self.send_message(chat, f'Тикет {ticket.id} был принят '
                                          f'оператором #{operator.id}')

        # отправляем сообщение пользователя и регистрируем его
        message = await self.send_message(chat, ticket.message_text)
        user_message = UserMessage.objects.get(ticket=ticket, ticket__user_id=ticket.user_id)
        user_message.support_message_id = message.id
        user_message.save()

        async for operator_user in self.iter_participants(chat):
            if not operator_user.is_self:
                operator_client = self.operator_clients[operator_user.id]
                await self._append_or_create_folder(operator_client, 'Opened', chat)
                await self._pop_or_delete_folder(operator_client, 'Closed', chat)

        ticket.is_ended = False
        ticket.support_chat_id = -abs(chat.id)
        ticket.operator = operator
        ticket.save()

        return True

    @check_connection
    async def get_dialog_story(self, ticket: Ticket):
        ids = []
        for message in ticket.messages.all():
            ids.append(message.support_message_id)

        return await self.get_messages(ids=ids, entity=ticket.support_chat_id)

    async def _get_or_create_chat(self, op_user_id, user_id) -> tuple[Chat, bool]:
        title = f'support_chat_{user_id}'

        async for chat in self.iter_dialogs():
            logger.debug(chat)
            if chat.name == title:
                return chat, False

        users = [op_user_id]
        # todo:
        managers = Operator.objects.filter(role='manager')
        for manager in managers:
            users.append(manager.user_id)

        updates = await self(functions.messages.CreateChatRequest(
            users=users, title=title
        ))
        return updates.chats[0], True

    @staticmethod
    async def _append_or_create_folder(client, folder_name, chat):
        async with client:
            input_peer_chat = InputPeerChat(abs(chat.id))
            dialog_filters = await client(GetDialogFiltersRequest())

            max_dialog_filter_id = 1
            for dialog_filter in dialog_filters:
                if dialog_filter.id > max_dialog_filter_id:
                    max_dialog_filter_id = dialog_filter.id

                if dialog_filter.title == folder_name:
                    dialog_filter.include_peers.append(input_peer_chat)

                    return await client(UpdateDialogFilterRequest(
                        dialog_filter.id,
                        dialog_filter
                    ))

            new_dialog_filter = DialogFilter(
                id=0,
                title=folder_name,
                pinned_peers=[],
                include_peers=[input_peer_chat],
                exclude_peers=[],
                emoticon='💬'
            )
            logger.debug(max_dialog_filter_id)
            return await client(UpdateDialogFilterRequest(
                max_dialog_filter_id + 1,
                new_dialog_filter
            ))

    @staticmethod
    async def _pop_or_delete_folder(client, folder_name, chat):
        async with client:
            input_peer_chat = InputPeerChat(abs(chat.id))
            dialog_filters = await client(GetDialogFiltersRequest())

            for dialog_filter in dialog_filters:
                if dialog_filter.title == folder_name:
                    try:
                        dialog_filter.include_peers.remove(input_peer_chat)
                    except ValueError as e:
                        logger.debug(e)

                    dialog_filter_id = dialog_filter.id
                    if len(dialog_filter.include_peers) == 0:
                        dialog_filter = None

                    return await client(UpdateDialogFilterRequest(
                        dialog_filter_id,
                        dialog_filter
                    ))


support_manager = SupportManager(
    auto_reconnect=True,
    loop=asyncio.get_event_loop()
)
