import loguru
from django import forms
from django.contrib import admin
from django.utils.html import format_html

from .models import Operator, SupportBot, TelegramAPI, Ticket


class OperatorForm(forms.ModelForm):
    def clean(self):
        if Operator.objects.filter(role='core').exists() and self.cleaned_data.get('role') == 'core':
            self.add_error('role', 'Ви не можете додати ще одного Core UserBot оператора')
            return

    class Meta:
        fields = '__all__'


class TelegramAPIForm(forms.ModelForm):
    def clean(self):
        if TelegramAPI.objects.first():
            self.add_error(None, 'Ви не можете додати ще одні налаштування TelegramAPI')
            return

    class Meta:
        fields = '__all__'


@admin.register(TelegramAPI)
class TelegramAPIAdmin(admin.ModelAdmin):
    form = TelegramAPIForm
    list_display = ('api_id', )


@admin.register(Operator)
class OperatorAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'name', 'role', 'phone', 'is_registered', 'is_active')
    exclude = ('user_id', 'name', 'string_session')
    list_filter = ('is_registered', 'is_active', 'role')
    search_fields = ('phone', 'name', 'user_id')
    form = OperatorForm

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['is_registered', ]
        if obj and obj.is_registered:
            readonly_fields.append('phone')
        return readonly_fields


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'message_text', 'get_operator', 'is_ended')
    readonly_fields = ('id', 'user_id', 'message_text', 'get_operator', 'is_ended')

    def has_add_permission(self, request):
        return False

    @admin.display(description='Оператор')
    def get_operator(self, obj):
        if obj and obj.operator:
            return format_html(f'<a href="/admin/tbot_support/operator/{obj.operator.id}">{obj.operator.id}</a>')
        else:
            return '-'


@admin.register(SupportBot)
class SupportBotAdmin(admin.ModelAdmin):
    list_display = ('token', 'webhook')
