import loguru
from django.http import HttpResponse
from loguru import logger
from telebot import TeleBot, types
from telebot.types import Update

from .manager import support_manager as sm

bot = TeleBot('1044470544:AAFeMUmD7ynbFNQCW3z9hCdLtpR8UgKf82k')
logger.debug('test bot inited')
bot.set_webhook('https://1b92-188-163-73-205.ngrok.io/test_bot_webhook/')


def test_bot_webhook(request):
    loguru.logger.debug('testbot: update')
    if request.META['CONTENT_TYPE'] == 'application/json':
        json_data = request.body.decode('utf-8')
        update = Update.de_json(json_data)
        bot.process_new_updates([update])

        return HttpResponse(status=200)
    else:
        return HttpResponse(status=402)


@bot.message_handler(commands=['start'])
def start(message: types.Message):
    user_id = message.from_user.id
    bot.send_message(user_id, 'Введите ваш вопрос')


@bot.message_handler(content_types=['text'])
@sm.question_handler
def question_handler(message, is_new_ticket):
    if is_new_ticket:
        bot.send_message(
            message.from_user.id,
            'Ok, wait for this',
            reply_to_message_id=message.message_id
        )


@sm.answer_handler
def answer_handler(user_id, message_id, answer):
    sent_message = bot.send_message(user_id, answer, reply_to_message_id=message_id)
    return sent_message.message_id
