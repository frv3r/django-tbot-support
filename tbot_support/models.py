from django.db import models


class TelegramAPI(models.Model):
    api_id = models.BigIntegerField()
    api_hash = models.CharField(max_length=150)

    class Meta:
        verbose_name = 'TelegramAPI'
        verbose_name_plural = 'TelegramAPI'


class Ticket(models.Model):
    user_id = models.BigIntegerField()
    message_text = models.TextField(max_length=4096)

    operator = models.ForeignKey('Operator', on_delete=models.SET_NULL, null=True, related_name='tickets')
    support_chat_id = models.BigIntegerField(null=True)
    is_ended = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Тікет'
        verbose_name_plural = 'Тікети'


class Operator(models.Model):
    role_choices = (
        ('operator', 'Оператор'),
        ('manager', 'Менеджер'),
        ('core', 'Core UserBot')
    )

    phone = models.CharField(max_length=20)
    role = models.CharField(max_length=30, choices=role_choices)

    string_session = models.TextField(max_length=500, null=True)

    name = models.CharField(max_length=100, null=True)
    user_id = models.BigIntegerField(null=True)
    is_registered = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Оператор'
        verbose_name_plural = 'Оператори'


class SupportBot(models.Model):
    token = models.CharField(max_length=150)
    webhook = models.CharField(max_length=150)

    class Meta:
        verbose_name = 'Бот операторів підтримки'
        verbose_name_plural = 'Боти операторів підтримки'


class TicketMessage(models.Model):
    """
    Модель отправленного сообщения операторам поддержки.
    Нужна для того чтобы после принятия тикета оператором, удалять кнопку "в роботу" у других операторов
    """

    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    operator = models.ForeignKey(Operator, on_delete=models.CASCADE)

    message_id = models.BigIntegerField()


class UserMessage(models.Model):
    """
    Модель для связи идентификаторов сообщений в чате поддержки и в чате пользователя
    """
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, related_name='messages')

    user_message_id = models.BigIntegerField(null=True)
    support_message_id = models.BigIntegerField(null=True)
